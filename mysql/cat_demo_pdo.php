<link rel="stylesheet" href="../basic/style.css"/>
<?php
require("../basic/lib.php");

$db = new PDO("mysql:host=localhost;dbname=n02k58;port=3306;charset=utf8mb4", "root", "")
    or die("Cannot connect to database");


$id = (int) $_GET["id"];

$statement = $db->prepare("SELECT * FROM cat WHERE id=:id");
$statement->execute(["id" => $id]);

if($statement->errorCode() > 0) {
    die("Error execute query: " . $statement->errorInfo()[2]);
}

$rows = [];
while($row = $statement->fetch()) {
    $rows[] = $row;
}

$columns = ["Tiêu đề nhóm tin" => "title", 
            "Mô tả nhóm tin" => "description"];
printArray($rows, "test-1", $columns);
?>