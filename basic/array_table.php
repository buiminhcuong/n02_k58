<?php
$a = array(
    array("name" => "Nguyen Van An", "address" => "Ha Noi"),
    array("name" => "Nguyen Van Binh", "address" => "Ha Giang"),
    array("name" => "Nguyen Van Chung", "address" => "Hai Phong")
);

?>

<h1>C1: HTML in PHP</h1>
<?php
include("lib.php");
printArray($a);
?>

<hr/>
<h1>C2: PHP in HTML</h1>

<table>
    <tr>
        <td>Name</td>
        <td>Address</td>
    </tr>
    <?php foreach($a as $value) { ?>
    <tr>
        <td><?= $value["name"] ?></td>
        <td><?= $value["address"] ?></td>
    </tr> 
    <?php } ?>
</table>