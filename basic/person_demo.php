<?php
class Person {
    private $name;
    private $address;

    function __construct($name, $address = "Viet Nam") {
        $this->name = $name;
        $this->address = $address;
    }

    function getInfo() {
        return "Person name: {$this->name}; Person address: {$this->address}";
    }
}

class Student extends Person {
    private $math;
    private $phys;
    private $chem;

    function __construct($name, $address, int $math, int $phys, int $chem) {
        parent::__construct($name, $address);
        $this->math = $math;
        $this->phys = $phys;
        $this->chem = $chem;
    }

    function getAvg() {
        return ($this->math + $this->phys + $this->chem) / 3;
    }
}



$p1 = new Person("Nguyen Van An", "Hanoi");

$persons = [
    new Person("Nguyen Van An", "Ha Noi"),
    new Person("Nguyen Van Binh","Ha Giang"),
    new Person("Nguyen Van Chung", "Hai Phong")
];

foreach($persons as $person) {
    echo("{$person->getInfo()}<br/>");
}

?>
