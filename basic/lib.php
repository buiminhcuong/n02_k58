<?php

function printArray($a, $className = "data-table",
                $columns = ["Ho ten" => "name", "address" => "address"]) {
    echo("<table class=\"$className\">
    <tr class=\"header\">");
    foreach($columns as $columnTitle => $columnName) {
        echo("<td>$columnTitle</td>");
    }
    echo("</tr>");
    $count = 0;
    foreach($a as $value) {
        $count++;
        $rowClass = $count % 2 == 1 ? "odd" : "even";
        echo("<tr class=\"row $rowClass\">");
        foreach($columns as $columnName) {
            $fieldValue = $value[$columnName];
            echo("<td>{$fieldValue}</td>");
        }
        echo("</tr>");
    }    
    echo("</table>");
}

?>