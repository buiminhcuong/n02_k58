<h1>Array decleration</h1>
<?php
    $a[0] = "xin";
    $a[1] = "chao";
    $a[2] = 5;

    echo("{$a[2]} <br/>");
    $a[] = 6; // ~ $a[3]
    $a[8] = 7;
    $a[] = 8; // ~ $a[9]
    $a[] = 9; // ~ $a[10]
    echo("{$a[3]} <br/>");
    $a["key1"] = "value1";
    echo("{$a["key1"]} <br/>");
    echo("<hr/>");

    $b = ["xin", "chao", 5, 6, 8 => 7, 8, 9, "key1" => "value1"];
    echo("{$b[8]} <br/>");
    echo("{$b["key1"]} <br/>");
    echo("<hr/>");

    $c = array("xin", "chao", 5, 6, 8 => 7, 8, 9, "key1" => "value1");
    echo("{$c[8]} <br/>");
    echo("{$c["key1"]} <br/>");
?>
<h1>Array processing</h1>
<?php
    foreach($b as $k => $v) {
        echo("key = $k; value = $v <br/>");
    }
    echo("<hr />");
    foreach($b as $v) {
        echo("value = $v <br/>");
    }
?>
