<script>
    fns = (function() {
        var result = [];
        for(var i = 0; i < 5; i++) {
            var inner = (function(i2) {
                return function() {
                    console.log(i2);
                }
            })(i);
            result.push(inner);
        }

        return result;
    })();
    
    fns[0]();
    fns[1]();
    fns[2]();
    fns[3]();
    fns[4]();
</script>