<script>
  //Creator/Higher Order Function (HOF)
  var increase = (function(initValue) {
    var count = initValue;

    return function(n) {
      return count+=n;
    }
  })(10);

  console.log(increase(5));
  count = 20;
  console.log(increase(7));
  console.log(increase(8));
</script>