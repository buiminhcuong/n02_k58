<?php
require("auth.php");
session_start();
$error = "";
if(isset($_POST["user"])) {
    $error = loginRedirect( $_POST["user"],  $_POST["password"], "admin.php");
}

?>
<html>
    <body>
        <?php echo($error) ?>
        <form method="POST">
            Username: <input type="text" name="user" />
            Password: <input type="password" name="password"/>
            <input type="submit" value="Login"/>
        </form>
    </body>
</html>