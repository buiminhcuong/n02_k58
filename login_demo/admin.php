<?php
require("auth.php");
session_start();
guardAuth("login.php");
?>

<html>
    <body>
        <div id="header">
            Admin page
            Hello <?= getLoggedInUser() ?>, <a href="logout.php">Logout</a>
        </div>
    </body>
</html>