<?php
function loginRedirect($user, $password, $page){  
    if($user == "admin" && $password == "123"){
        $_SESSION["username"] = $user;
        header("location:$page");
    }
    else {
        return "invalid username or password!";
    }
    return "";
}

function getLoggedInUser() {
    if(!isset($_SESSION["username"])){
        return "";
    }
    else {
        return $_SESSION["username"];
    }
}

function guardAuth($redirectPage) {
    if(getLoggedInUser() == "") {
        header("location:$redirectPage");
    }
}

function logout($redirectPage) {
    session_destroy();
    header("location:$redirectPage");
}


?>